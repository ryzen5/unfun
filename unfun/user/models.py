from django.db import models


# Create your models here.

class User(models.Model):
    id = models.BigAutoField(primary_key=True, auto_created=True)
    username = models.CharField(max_length=10)
    email = models.EmailField()
    password = models.CharField(max_length=128)
    date_of_join = models.DateTimeField()
    admin = models.BooleanField()
    active = models.BooleanField()
    code = models.CharField(max_length=256)
