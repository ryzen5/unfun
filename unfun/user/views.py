import datetime
import hashlib
import random
import re

from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .models import User


# Create your views here.
def register(request):
    return render(request, "user/templates/register.html", context={"random": random.randint(1111, 9999)})


def login(request):
    return render(request, "user/templates/login.html")


def home(request):
    return HttpResponse(request.session.get("username"))
def verify(request):
    if request.method == "POST":
        if request.GET:
            '''
            ##############################################
            #               For registration             #
            ##############################################
            '''
            if request.GET.get("q") == "register":

                email_regex = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
                if request.POST.get("username") is not None and \
                                        12 >= len(request.POST.get("username")) >= 6:
                    if request.POST.get("password") is not None and \
                                    len(request.POST.get("password")) >= 8:
                        if request.POST.get("email") is not None and \
                                re.match(email_regex, request.POST.get("email")):
                            if not User.objects.filter(
                                    username=request.POST.get("username")).exists() and not User.objects.filter( \
                                    email=request.POST.get("email")).exists():
                                verify_code = hashlib.sha512(str(random.randint(100000, 999999)).encode()).hexdigest()

                                # TODO Add more Info.
                                new_user = User(username=request.POST.get("username"),
                                                password=hashlib.sha512(
                                                    request.POST.get("password").encode()).hexdigest(),
                                                email=request.POST.get("email"),
                                                date_of_join=datetime.datetime.now(),
                                                admin=False,
                                                active=False,
                                                code=verify_code)
                                new_user.save()

                                # TODO Add an email template.
                                send_mail("Verify your account.",
                                          "Please visit localhost:8000/verify?q=verify&code?=%s" % verify_code,
                                          "support@website.com",
                                          [request.POST.get("email")])
                                return HttpResponse("Success, you are now registered")
                            else:
                                return HttpResponse("Already registered e-mail or username.")
                        else:
                            return HttpResponse("Malformed Email")
                    else:
                        return HttpResponse("Malformed Password")
                else:
                    return HttpResponse("Malformed Username")
            elif request.GET.get("q") == "login":
                '''
                ##############################################
                #               For login                    #
                ##############################################
                '''
                if request.POST.get("username") and request.POST.get("password"):
                    if User.objects.filter(username=request.POST.get("username")).exists():
                        user = User.objects.filter(username=request.POST.get("username")).get()
                        if user.password == hashlib.sha512(request.POST.get("password").encode()).hexdigest():
                            request.session["id"] = user.id
                            request.session["username"] = user.username
                            request.session["email"] = user.email
                            return redirect("/home/")
                        else:
                            return HttpResponse("Wrong Password or Username")
                    else:
                        return HttpResponse("Username doesn't exist")
                else:
                    return "Username or password not given"
            else:
                return HttpResponse("No query specified")
        else:
            return HttpResponse("Please specify action")
    elif request.method == "GET":

        '''
        #################################
        #This is for email verification.# 
        #################################
        '''
        if request.GET.get("q") and request.GET.get("q") == "verify" and request.GET.get("code"):
            if User.objects.filter(code=request.GET.get("code")).exists():
                user = User.objects.filter(code=request.GET.get("code")).get()
                user.active = True
                user.save()
                return HttpResponse("Account is now active")
            else:
                return HttpResponse("User not found")
    else:
        return HttpResponse("Invalid request")
