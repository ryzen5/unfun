function isEmail(email) {

  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);

}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function register(url_to_request, json_data) {

    if(json_data.username.length >= 6  && json_data.username.length <= 12){

        if(json_data.password.length >= 8){

            if(isEmail(json_data.email)){
                var recv_data = "Error, please try again.";
                $.ajax({
                   url: url_to_request,
                   type: 'POST',
                   async: false,
                   data: json_data,
                   headers: {
                       "X-CSRFToken": json_data.token
                   },
                   dataType: 'text',
                   success: function(data) {
                       recv_data = data;
                       console.log(data);

                   },
                   error: function(data) {
                       recv_data = "Account creation failed";
                       console.log(data);

                   }

               });
               return recv_data;
            }
            else {
                return "Invalid Email";
            }
         }
         else{
            return "Password should contain at-least 8 characters";
         }
    }
    else {
        return "Username should have at-least 6 and max 12 chars.";
    }
}


